#include "doomkeys.h"

#include "doomgeneric.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
/* #include <sys/time.h> */

#include <kernel/video.h>
#include <syscall.h>
#include <fcntl.h>

#define KEYQUEUE_SIZE 16

static unsigned short s_KeyQueue[KEYQUEUE_SIZE];
static unsigned int s_KeyQueueWriteIndex = 0;
static unsigned int s_KeyQueueReadIndex = 0;

FILE *fbfile = NULL;
static uint32_t *fb = (void *)0x70000000;
size_t fb_width, fb_height;

static unsigned char convertToDoomKey(unsigned int key)
{
    switch (key)
    {
    case 0x5a:
        key = KEY_ENTER;
        break;
    case 0x08:
        key = KEY_ESCAPE;
        break;
    case 0x61:
        key = KEY_LEFTARROW;
        break;
    case 0x6a:
        key = KEY_RIGHTARROW;
        break;
    case 0x63:
        key = KEY_UPARROW;
        break;
    case 0x60:
        key = KEY_DOWNARROW;
        break;
    case 0x11:
    case 0x58:
        key = KEY_FIRE;
        break;
    case 0x29:
        key = KEY_USE;
        break;
    case 0x12:
    case 0x59:
        key = KEY_RSHIFT;
        break;
    case 0x35:
        key = 'y';
        break;
    default:
        key = 0;
        break;
    }

    return key;
}

static void addKeyToQueue(int pressed, unsigned int keyCode)
{
    unsigned char key = convertToDoomKey(keyCode);

    unsigned short keyData = (pressed << 8) | key;

    s_KeyQueue[s_KeyQueueWriteIndex] = keyData;
    s_KeyQueueWriteIndex++;
    s_KeyQueueWriteIndex %= KEYQUEUE_SIZE;
}

void DG_Init()
{
    memset(s_KeyQueue, 0, KEYQUEUE_SIZE * sizeof(unsigned short));

    fbfile = fopen("/dev/fb0", "r");
    syscall_ioctl(fbfile->fd, IOCTL_VID_MAP, &fb);

    syscall_ioctl(fbfile->fd, IOCTL_VID_WIDTH, &fb_width);
    syscall_ioctl(fbfile->fd, IOCTL_VID_HEIGHT, &fb_height);
}

static uint32_t kbd_bitset[8] = {0};
#define KBD_IDX(k) (k / (8*sizeof *kbd_bitset))
#define KBD_OFF(k) (k % (8*sizeof *kbd_bitset))
static void kbd_set(uint8_t k) {
    uint32_t index = KBD_IDX(k),
             offset = KBD_OFF(k);
    kbd_bitset[index] |= 1<<offset;
}

static int kbd_get(uint8_t k) {
    uint32_t index = KBD_IDX(k),
             offset = KBD_OFF(k);
    return (kbd_bitset[index] & 1<<offset) ? 1 : 0;
}

static void kbd_clear(uint8_t k) {
    uint32_t index = KBD_IDX(k),
             offset = KBD_OFF(k);
    kbd_bitset[index] &= ~(1<<offset);
}

static void handle_kbd(void) {
    int pressed = 1;
    uint8_t c;
    while((c = fgetc(stdin)) < 0) {
        if(c == 0xf0) {
            pressed = 0;
            continue;
        }
        if(kbd_get(c) != pressed) {
            addKeyToQueue(pressed, c);
            if(pressed) kbd_set(c);
            else kbd_clear(c);
        }
        pressed = 1;
    }
}

void DG_DrawFrame()
{
    if(fbfile)
        for(int i = 0; i < DOOMGENERIC_RESY; i++)
            memcpy(fb + i*fb_width, DG_ScreenBuffer + i * DOOMGENERIC_RESX, DOOMGENERIC_RESX * 4);

    handle_kbd();
}

void DG_SleepMs(uint32_t ms)
{
    uintmax_t start, now;
    syscall_sysfunc(4, &start);
    do syscall_sysfunc(4, &now);
    while(now - start < 1000);
}

uint32_t DG_GetTicksMs()
{
    /* struct timeval  tp; */
    /* struct timezone tzp; */

    /* gettimeofday(&tp, &tzp); */
    uintmax_t now;
    syscall_sysfunc(4, &now);

    return now;
    /* return (tp.tv_sec * 1000) + (tp.tv_usec / 1000); /1* return milliseconds *1/ */
}

int DG_GetKey(int* pressed, unsigned char* doomKey)
{
    if (s_KeyQueueReadIndex == s_KeyQueueWriteIndex)
    {
        //key queue is empty

        return 0;
    }
    else
    {
        unsigned short keyData = s_KeyQueue[s_KeyQueueReadIndex];
        s_KeyQueueReadIndex++;
        s_KeyQueueReadIndex %= KEYQUEUE_SIZE;

        *pressed = keyData >> 8;
        *doomKey = keyData & 0xFF;

        return 1;
    }
}

void DG_SetWindowTitle(const char * title)
{

}

int main(int argc, char **argv)
{
    syscall_open("/dev/kbd", O_RDONLY, 0); /* fd 0: stdin */
    syscall_open("/dev/console", O_WRONLY, 0); /* fd 1: stdout */
    syscall_open("/dev/console", O_WRONLY, 0); /* fd 2: stderr */

    doomgeneric_Create(argc, argv);

    for (int i = 0; ; i++)
    {
        doomgeneric_Tick();
    }
    

    return 0;
}
